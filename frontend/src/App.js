import React, { useState, useEffect }  from 'react';
import logo from './logo.svg';
import './App.css';
import Post from './components/Post'
import Container from '@material-ui/core/Container'
import SiteAppBar from './components/SiteAppBar'
import Feed from './components/Feed'

function App () {
  const post1 = [<Post title="test" 
  embed_link="https://www.youtube.com/embed/H4KmSL5vl5o"
  content=""
  datePosted="2020-08-26"/>];

  const [hasError, setErrors] =  useState(false);
  const [items, setItems] = useState({results:[]});

  const apiURL = "http://localhost:8000/feed/";

  async function fetchData() {
    const res = await fetch(apiURL);
    res.json()
      .then(res => setItems(res))
      .catch(err => setErrors(err));
  }

  useEffect(() => {
    fetchData();
  });

  return (
    <div className="App">
      <SiteAppBar/>
      <Container maxWidth="sm">
        <Feed posts={items.results}/>
        
      </Container>
    </div>
  );
}

export default App;
