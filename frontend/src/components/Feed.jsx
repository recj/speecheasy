import React from 'react'
import Post from './Post'

export default function Feed(props) {
    return (
        <div className="feed">
          {props.posts.map(post => (
            <Post title={post.title} embedLink={post.embed_link} datePosted={post.date_posted}/>
          ))}
        </div>
      );
}