from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class Post(models.Model):
    LANGUAGE_TYPES = (
        ('EN', 'English'),
        ('JP', 'Japanese'),
        ('FR', 'French'),
        ('SP', 'Spanish'),
        ('PT', 'Portguese'),
        ('CN', 'Chinese'),
        ('KR', 'Korean'),
        ('DE', 'German'),
    )
    title = models.TextField(max_length=150)
    embed_link = models.URLField(default=None, max_length=300)
    content = models.TextField(max_length=150, blank=True)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    language = models.CharField(max_length=2, choices=LANGUAGE_TYPES)

    @property
    def number_of_comments(self):
        return Comment.objects.filter(post_connected=self).count()
    
    @property
    def number_of_favorites(self):
         return Favorite.objects.filter(post_connected=self).count()

    def favorited_by(self, user):
        return Favorite.objects.filter(post_id=post_id, user=user).exists()
    
class Favorite(models.Model):
    favoriter = models.ForeignKey(User, on_delete=models.CASCADE)
    post_connected = models.ForeignKey(Post, on_delete=models.CASCADE)

class Bookmark(models.Model):
    bookmarker = models.ForeignKey(User, on_delete=models.CASCADE)
    post_connected = models.ForeignKey(Post, on_delete=models.CASCADE)
    date_bookmarked = models.DateTimeField(default=timezone.now)

class Comment(models.Model):
    content = models.TextField(max_length=150)
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    post_connected = models.ForeignKey(Post, on_delete=models.CASCADE)
